# weather_alert_sms
I created this short script in light of the recent floods at Chennai. I made it for a friend who was stuck there to give updates via sms.

The sms updates were constructed as follows:
  First sms at midnight (contains forcast for):
      08:30, 11:30, 14:30, 17:30, 20:30
  Each will be a 3 hour forcast.
  The format of the Text message recived will be:
    <date>
    08:30-> {forcast}(<rain_in_cm>)
    11:30-> {forcast}(<rain_in_cm>)
    14:30-> {forcast}(<rain_in_cm>)
    17:30-> <forcast>(<rain_in_cm>)
    20:30-> <forcast>(<rain_in_cm>)
    
  Then 3 Messages at 1300, 1600 and 2200 hrs (These will contain the Current weather conditions)
  Format:
   {Forcast}, with {<%>} hum; temp:{}; wind={}
   

For text message I am using twilio and way2sms
and for weather I am using OpenWeather API

Feel free to use the code/modify it.
