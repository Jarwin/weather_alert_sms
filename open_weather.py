#!/usr/bin/env python
import auth
import urllib
import json
from datetime import datetime
from send_msg import Way2sms
from send_msg import Send_message_twilio
import time
class Weather_data():
	current_weather = "http://api.openweathermap.org/data/2.5/weather?id=1264527&appid="+auth.api_key()
	three_hour = "http://api.openweathermap.org/data/2.5/forecast?id=1264527&appid="+auth.api_key()
	msg_response = ""
	number = ""
	def __init__(self, num):
			self.number = num
			self.timer()

	def request_api(self, id):
		if id == 1:
				call_api = urllib.urlopen(self.current_weather)
				return json.loads(call_api.read())
		if id == 2:
				call_api = urllib.urlopen(self.three_hour)
				return json.loads(call_api.read())

	def timefmt(self, epoch):
		return time.strftime("%H:%M", time.localtime(epoch))

	def get_data(self, id):
		data = self.request_api(id)
		if id==1:
			weather = data['weather'][0]['main']
			short_decp = weather = data['weather'][0]['description']
			humidity = data['main']['humidity']
			temp = data['main']['temp']-274.15
			wind = data['wind']['speed']
			return "{} ({}) with {}% hum; temp:{}; wind={}".format(weather, short_decp, humidity, temp, wind)
		if id==2:
			first, second, third, fourth, fifth = data['list'][2], data['list'][3], data['list'][4], data['list'][5], data['list'][6]
			time1, time2, time3, time4, time5 = self.timefmt(data['list'][2]['dt']), self.timefmt(data['list'][3]['dt']), self.timefmt(data['list'][4]['dt']), self.timefmt(data['list'][5]['dt']), self.timefmt(data['list'][6]['dt'])
			weather1, weather2, weather3, weather4, weather5 = data['list'][2]['weather'][0]['description'], data['list'][3]['weather'][0]['description'], data['list'][4]['weather'][0]['description'], data['list'][5]['weather'][0]['description'], data['list'][6]['weather'][0]['description']
			rain1, rain2, rain3, rain4, rain5 = str(data['list'][2]['rain']['3h'])+"cm", str(data['list'][3]['rain']['3h']), str(data['list'][4]['rain']['3h']), str(data['list'][5]['rain']['3h']), str(data['list'][6]['rain']['3h'])
			todayDate = time.strftime("%dnd", time.localtime())
			return "{}\n{}->{}({})\n{}->{}({})\n{}->{}({})\n{}->{}({})\n{}->{}({})".format(todayDate,time1, weather1, rain1,time2, weather2, rain2,time3, weather3, rain3,time4, weather4, rain4,time5, weather5, rain5)

	def send(self):
		msg = self.get_data(1)
		way = Way2sms(msg, self.number)
		twilio = Send_message_twilio(self.number, msg)
		print msg
		print len(msg)


	def at_midnight(self):
		data = self.get_data(2)
		print data
		way = Way2sms(data, self.number)
		twilio = Send_message_twilio(self.number, data)
		print len(data)

	def timer(self):
		#datetime.now()
		value = input("1 or 2?")
		if value == 1:
			self.send()
			now = datetime.now()
			mid = now.replace(hour=23, minute=59)
			time_left = mid - now
			time_left = int(time_left.total_seconds())+10
			print "sleeping for: ", time_left
			self.recursive_counter
			time.sleep(time_left)
		elif value == 2:
			self.at_midnight()
			self.recursive_counter()

	def recursive_counter(self):
		time.sleep(46800)
		self.send()
		time.sleep(10800)
		self.send()
		time.sleep(21600)
		self.send()
		time.sleep(7560)
		self.at_midnight
		self.recursive_counter()



a = Weather_data("8800574117")
#a.send("8800574117")
#a.at_midnight()